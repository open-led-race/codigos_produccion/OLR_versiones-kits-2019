#include <Otto9Humanoid.h> //-- Otto Library version 9
Otto9Humanoid Otto;  //This is Otto!
#include <Servo.h>
// SERVO PINs //////////////////////////////////////////////////////////////////////////////
#define PIN_YL 2 //servo[0]  left leg
#define PIN_YR 3 //servo[1]  right leg
#define PIN_RL 4 //servo[2]  left foot
#define PIN_RR 5 //servo[3]  right foot
#define PIN_LA 6 //servo[4]  Left arm if enabled
#define PIN_RA 7 //servo[5]  Right arm if enabled
// ULTRASONIC PINs /////////////////////////////////////////////////////////////////////////
#define PIN_Trigger  8  //TRIGGER pin (8)
#define PIN_Echo     9  //ECHO pin (9)
// BUZZER PIN //////////////////////////////////////////////////////////////////////////////
#define PIN_Buzzer  13 //BUZZER pin (13)
// SOUND SENSOR PIN //////////////////////////////////////////////////////////////////////////
#define PIN_NoiseSensor A6  //SOUND SENSOR   ANALOG pin (A6)
// LED MATRIX PINs //////////////////////////////////////////////////////////////////////////
#define DIN_PIN    A3   //DIN pin (A3)
#define CS_PIN     A2   //CS pin (A2)
#define CLK_PIN    A1   //CLK pin (A1)
#define LED_DIRECTION  2// LED MATRIX CONNECTOR position (orientation) 1 = top 2 = bottom 3 = left 4 = right  DEFAULT = 1




Servo servo_red;
Servo servo_green; 
char brx;
String txt="";

void setup() {
  // initialize serial:
  Serial.begin(115200);
  servo_red.attach(6);  
  servo_green.attach(7);  
  servo_red.write(180);     
  servo_green.write(0);  
 Otto.initHUMANOID(PIN_YL, PIN_YR, PIN_RL, PIN_RR, PIN_LA, PIN_RA, true, PIN_NoiseSensor, PIN_Buzzer, PIN_Trigger, PIN_Echo); //Set the servo pins and ultrasonic pins
  Otto.initMATRIX( DIN_PIN, CS_PIN, CLK_PIN, LED_DIRECTION);   // set up Matrix display pins = DIN pin,CS pin, CLK pin, MATRIX orientation 
  
  Otto.matrixIntensity(1);// set up Matrix display intensity
  //Otto wake up!
  Otto.sing(S_connection);
  Otto.home();
  // Animation Uuuuuh - A little moment of initial surprise
  //-----
  
  for (int i = 0; i < 2; i++) {
    for (int i = 0; i < 8; i++) {
      Otto.putAnimationMouth(littleUuh, i);
      delay(150);
    }
  }
  //Smile for a happy Otto :)
  Otto.putMouth(smile);
  Otto.sing(S_happy);
  delay(200);
  
}

void loop() {
  // if there's any serial available, read it:
  while (Serial.available() > 0) {
    brx=Serial.read();
    // look for the newline. That's the end of your sentence:
    if (brx == '\n') {
      // constrain the values to 0 - 255 and invert 
      if (txt.equals("w2\r")==true) {
                                  servo_red.write(180);     
                                  servo_green.write(90);
                                  Otto.putMouth(bigSurprise);
                                  Otto.sing(S_surprise);
                                  //Otto.jump(5, 500);
                                  Otto.putMouth(confused);
                                  Otto.sing(S_cuddly);
                                  }
      if (txt.equals("w1\r")==true) {
                                  servo_red.write(90);     
                                  servo_green.write(0);
                                  Otto.putMouth(bigSurprise);
                                  Otto.sing(S_surprise);
                                  //Otto.jump(5, 500);
                                  Otto.putMouth(confused);
                                  Otto.sing(S_cuddly);} 
      if (txt.equals("r4\r")==true) {
                                  servo_red.write(180);     
                                  servo_green.write(0);
                                  Otto.putMouth(happyOpen);
                                  Otto.sing(S_happy_short);}                                 
      if (txt.equals("r5\r")==true) {
                                  servo_red.write(90);     
                                  servo_green.write(90);
                                  //Otto.jump(5, 500);
                                  Otto.sing(S_cuddly);}                                                            
      txt="";                                     
    }
    else{txt.concat(brx);
        }
  }
}
