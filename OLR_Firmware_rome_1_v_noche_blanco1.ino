/*
 * ____                     _      ______ _____    _____
  / __ \                   | |    |  ____|  __ \  |  __ \
 | |  | |_ __   ___ _ __   | |    | |__  | |  | | | |__) |__ _  ___ ___
 | |  | | '_ \ / _ \ '_ \  | |    |  __| | |  | | |  _  // _` |/ __/ _ \
 | |__| | |_) |  __/ | | | | |____| |____| |__| | | | \ \ (_| | (_|  __/
  \____/| .__/ \___|_| |_| |______|______|_____/  |_|  \_\__,_|\___\___|
        | |
        |_|
 Open LED Race
 An minimalist cars race for LED strip

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 by gbarbarov@singulardevices.com  for Arduino day Seville 2019

 Code made dirty and fast, next improvements in:
 https://github.com/gbarbarov/led-race
*/


#include <Adafruit_NeoPixel.h>

#define PIN_LED        2//A0  // R 500 ohms to DI pin for WS2812 and WS2813, for WS2813 BI pin of first LED to GND  ,  CAP 1000 uF to VCC 5v/GND,power supplie 5V 2A
#define PIN_P1         A0  //6  // switch player 1 to PIN and GND
#define PIN_P2         A1  //7   //A3  // switch player 2 to PIN and GND
#define PIN_AUDIO      3   // through CAP 2uf to speaker 8 ohms
#define PIN_VCC_ADC1   A4   // switch player 1 to PIN and GND //anulado  forzado a 5 V
#define PIN_VCC_ADC2   A3   // switch player 2 to PIN and GND //anulado  forzado a 5 V
#define PIN_SP1        8   // status switch player 1
#define PIN_SP2        9   // status switch player 2
#define PIN_REL1       5   // rele 1
#define PIN_REL2       4   // rele 2
//#define SPEED_TAUX     0.8 //botones y tablaito
#define SPEED_TAUX     0.5   //botones y tablaito
#define LED_START_LIGHT 6
#define NLEDS_FX_WIN 50

int NPIXELS=300;//253; // leds on track
int NLED_AUX_TRACK=47 ; // 50cm 
int NLED_TUNNEL_IN_TRACK= 0; // 50cm
int NLED_TUNNEL_IN_RING= 0;
int NLED_TUNNEL_OUT= 0; // 50cm
int NLED_TUNNEL_OUT_RING= 0;
int LED_START= 30;

int MIN_ADC1= 600;
int MAX_ADC1= 800;

int MIN_ADC2= 600;
int MAX_ADC2= 800;


byte PHYSIC=0; 

byte HIGH_RAMP=10;
byte SELECT_TRACK_AUX=0;

int INI_TRACK_AUX=300-47;

//int INI_TRACK_AUX=46;
int END_TRACK_AUX=21;  // en este caso la salida del aux no coincide con la longitud del track aux

int LED_BOX_STOP1=35;
int LED_BOX_STOP2=55;

int LED_SPEED_COIN =- 1;

bool updatePanel = false;
int MODO_FLAMENCO  = 1;

#define TRACK_MAIN   1
#define TRACK_AUX    2
#define TRACK_IN     3
#define TRACK_OUT    4

#define MAXLED     253+47  // 466 MAX LEDs actives on strip


#define COLOR1    track.Color(255,0,0)
#define COLOR2    track.Color(0,255,0)

int win_music[] = {
  2637, 2637, 0, 2637,
  0, 2093, 2637, 0,
  3136
};


byte  gravity_map[MAXLED];

int TBEEP=3;

enum ctr_type{
  NOT_DEFINED = 0,
  DIGITAL_MODE,
  ANALOG_MODE,
};

typedef struct{
  enum ctr_type mode;
  int8_t pin;
  int adc;
  int badc;
  int delta_analog;
  byte flag_sw;
  int8_t pin_status;
}controller_t;


typedef struct{
  controller_t* ct;
  float speed;
  float fuel;
  float dist;
  float dist_aux;
  float dist_in;
  float dist_out;
  byte loop;
  uint32_t color;
  int trackID;
}car_t;

static car_t car1;
static car_t car2;


static controller_t switch1;
static controller_t switch2;

byte leader=0;
byte loop_max=5; //total laps race

float ACEL=0.2;
float kf=0.025; //friction constant
float kg=0.003; //gravity constant

byte draworder=0;
char msg[50];
enum {
  DELTA_ANALOG = 2,
};

unsigned long int T_SPEED_COIN;
unsigned long timestamp=0;

Adafruit_NeoPixel track = Adafruit_NeoPixel(MAXLED, PIN_LED, NEO_GRB + NEO_KHZ800);

int tdelay = 5;

void init_controller( controller_t* ct, enum ctr_type mode, int pin,int pin_status );
byte get_controllerStatus( controller_t* ct );
void init_car( car_t* car, controller_t* ct, byte color, float fuel );


void set_ramp(byte H,byte a,byte b,byte c)
{for(int i=0;i<(b-a);i++){gravity_map[a+i]=127-i*((float)H/(b-a));};
 gravity_map[b]=127;
 for(int i=0;i<(c-b);i++){gravity_map[b+i+1]=127+H-i*((float)H/(c-b));};
}

void set_loop(byte H,byte a,byte b,byte c)
{for(int i=0;i<(b-a);i++){gravity_map[a+i]=127-i*((float)H/(b-a));};
 gravity_map[b]=255;
 for(int i=0;i<(c-b);i++){gravity_map[b+i+1]=127+H-i*((float)H/(c-b));};
}


void setup() {

  Serial.begin(115200);
  pinMode(PIN_SP1, OUTPUT);
  pinMode(PIN_SP2, OUTPUT);
  digitalWrite(PIN_SP1,LOW);
  digitalWrite(PIN_SP2,LOW);
  pinMode(PIN_REL1, OUTPUT);
  pinMode(PIN_REL2, OUTPUT);
  digitalWrite(PIN_REL1,HIGH);
  digitalWrite(PIN_REL2,HIGH);


  //DIGITAL_MODE = false; // fuerzo outputs 

  //if( DIGITAL_MODE == false ){
    pinMode(PIN_VCC_ADC1, OUTPUT);
    pinMode(PIN_VCC_ADC2, OUTPUT);
    digitalWrite(PIN_VCC_ADC1, HIGH);
    digitalWrite(PIN_VCC_ADC2, HIGH);
 /* }
  else{
    pinMode(PIN_P1, INPUT_PULLUP);
    pinMode(PIN_P2, INPUT_PULLUP);
  }
*/
 // pinMode(PIN_P1, INPUT_PULLUP); //pull up in adc
 // pinMode(PIN_P2, INPUT_PULLUP);


  //test adc
  /*while (true)
  {
  Serial.print( analogRead(PIN_P1));
  Serial.print(",");  
  delay(20);
  Serial.println( analogRead(PIN_P2));
  delay(20);
  }
  */
 

  for(int i=0;i<NPIXELS;i++) { gravity_map[i]=127; }

  init_car( &car1, &switch1, COLOR1 ,100);
  init_car( &car2, &switch2, COLOR2 ,100);
  
  //init_controller( &switch1, DIGITAL_MODE, PIN_P1 ,PIN_SP1);
  //init_controller( &switch2, DIGITAL_MODE, PIN_P2 ,PIN_SP2);

  init_controller( &switch1, ANALOG_MODE, PIN_P1 ,PIN_SP1);
  init_controller( &switch2, ANALOG_MODE, PIN_P2 ,PIN_SP2);


  track.begin();
 //if ( digitalRead(PIN_P1) == 0 ) {   //push switch 1 on reset for activate physic

 /*if ( ( digitalRead(PIN_P1) == 1 ) && (digitalRead(PIN_P2) == 1 ) )  {init_controller( &switch1, DIGITAL_MODE, PIN_P1 ,PIN_SP1);
                                                                      init_controller( &switch2, DIGITAL_MODE, PIN_P2 ,PIN_SP2);
                                                                      HIGH_RAMP=12;//16
                                                                      ACEL=0.2;
                                                                     }  //push switch 1 on reset for activate modo digital
                                                                     else {init_controller( &switch1, ANALOG_MODE, PIN_P1 ,PIN_SP1);
                                                                           init_controller( &switch2, ANALOG_MODE, PIN_P2 ,PIN_SP2);
                                                                           HIGH_RAMP=23;
                                                                           ACEL=0.150;
                                                                          };                                                                     
  */
 
  if ( (PHYSIC) == 1 ) {                
    set_ramp(HIGH_RAMP,84,94,104);    // ramp centred in LED 100 with 10 led fordward and 10 backguard
    for(int i=0;i<NPIXELS;i++){ track.setPixelColor(i, track.Color(0,0,(127-gravity_map[i])/8) ); }
    track.show();
  }
  start_race();
}


void start_race(){for(int i=0;i<NPIXELS;i++){track.setPixelColor(i,0);};
                  track.show();
                  delay(2000);
                  track.setPixelColor(LED_START_LIGHT, track.Color(255,0,0));
                 // track.setPixelColor(11, track.Color(0,255,0));
                  track.show();
                  tone(PIN_AUDIO,400);
                  delay(2000);
                  noTone(PIN_AUDIO);
                  //track.setPixelColor(12, track.Color(0,0,0));
                  //track.setPixelColor(11, track.Color(0,0,0));
                  track.setPixelColor(LED_START_LIGHT, track.Color(255,255,0));
                  //track.setPixelColor(9, track.Color(255,255,0));
                  track.show();
                  tone(PIN_AUDIO,600);
                  delay(2000);
                  noTone(PIN_AUDIO);
                  //track.setPixelColor(9, track.Color(0,0,0));
                  //track.setPixelColor(10, track.Color(0,0,0));
                  track.setPixelColor(LED_START_LIGHT, track.Color(0,255,0));
                  //track.setPixelColor(7, track.Color(255,0,0));
                  track.show();
                  tone(PIN_AUDIO,1200);
                  delay(2000);
                  noTone(PIN_AUDIO);
                  timestamp=0;
                  T_SPEED_COIN=millis()+random(5000,15000);
                  updatePanel = true;
                 };

void winner_fx() {
               int msize = sizeof(win_music) / sizeof(int);
               for (int note = 0; note < msize; note++) {
               tone(PIN_AUDIO, win_music[note],200);
               delay(230);
               noTone(PIN_AUDIO);
               updatePanel = false;
}
              };


void loop() {
   //tone(PIN_AUDIO,(30+((word)speed2*800)));
    for( int i=0; i<NPIXELS; i++){
      track.setPixelColor(i, track.Color(0,0,0));
    }
    //for(int i=0;i<=NPIXELS;i++){track.setPixelColor(i, track.Color(0,0,(127-gravity_map[i])/8) );};
    for( int i=0; i<NLED_AUX_TRACK; i++){
      track.setPixelColor(NPIXELS+i, track.Color(0,0,0) );
    }

    if (LED_SPEED_COIN > 0 )
      track.setPixelColor( 1+NPIXELS+LED_SPEED_COIN, track.Color(0,0,250) );
    else if (millis()>T_SPEED_COIN)
      LED_SPEED_COIN=random( 20, NLED_AUX_TRACK-20 );

    update_track( &car1 );
    if ( car1.trackID == TRACK_AUX )  process_aux_track( &car1 );
    if ( car1.trackID == TRACK_MAIN ) process_main_track( &car1 );
     
    update_track( &car2 );   
    if ( car2.trackID == TRACK_AUX )  process_aux_track( &car2 );
    if ( car2.trackID == TRACK_MAIN ) process_main_track( &car2 );

    if ( car1.dist > car2.dist ) leader=1;
    else if ( car2.dist > car1.dist ) leader=2;

    if ( car1.dist-LED_START > NPIXELS*car1.loop ) {
      car1.loop++;
      //sprintf(msg, "$%d&%d%%", car1.loop, car2.loop );
      //Serial.print( msg );
      updatePanel = true;
      tone(PIN_AUDIO,600);
      TBEEP=2;
    }
    if ( car2.dist-LED_START > NPIXELS*car2.loop ) {
      car2.loop++;
      updatePanel = true;
      tone(PIN_AUDIO,700);
      TBEEP=2;
    }

  /*Serial.print( analogRead(PIN_P1));
  Serial.print(",");  
  delay(20);
  Serial.println( analogRead(PIN_P2));
  delay(20);
 */

if( updatePanel ){
    updatePanel= false;
    sprintf(msg, "$%d&%d%%", car1.loop, car2.loop );
    Serial.print( msg );
  }
     if (car1.loop>loop_max) {for(int n=NPIXELS/4;n>0;n--) {for(int i=0;i<NLEDS_FX_WIN;i++){track.setPixelColor(i, track.Color((i+8),0,0));}; track.show();}
                                                    winner_fx();car1.loop=0;car2.loop=0;car1.dist=0;car2.dist=0;car1.speed=0;car2.speed=0;timestamp=0;
                                                    car1.trackID=TRACK_MAIN;car2.trackID=TRACK_MAIN;
                                                    start_race();
                                                   }
     if (car2.loop>loop_max) {for(int n=NPIXELS/4;n>0;n--) {for(int i=0;i<NLEDS_FX_WIN;i++){track.setPixelColor(i, track.Color(0,(i+8),0));}; track.show();} // track.Color(0,255/(i/4+1),0))
                                                    winner_fx();car1.loop=0;car2.loop=0;car1.dist=0;car2.dist=0;car1.speed=0;car2.speed=0;timestamp=0;
                                                    car1.trackID=TRACK_MAIN;car2.trackID=TRACK_MAIN;
                                                    start_race();
                                                   }         

    if ((millis() & 512)==(512*draworder)) {if (draworder==0) {draworder=1;}
                          else {draworder=0;}
                         };
    if (round(car1.speed*100)>round(car2.speed*100)) {draworder=1;};
    if (round(car2.speed*100)>round(car1.speed*100)) {draworder=0;};
       
    if ( draworder==0 ) {
      draw_car( &car1 );
      draw_car( &car2 );
    }
    else {
      draw_car( &car2 );
      draw_car( &car1 );
    }

    track.show();
    delay(tdelay);
    //Serial.println(analogRead(PIN_P1));
    //Serial.print(T_SPEED_COIN);
    //Serial.print(" ");
    //Serial.println(millis());

    if (TBEEP>0) {
      TBEEP-=1;
      if (TBEEP==0) {
        noTone(PIN_AUDIO); // lib conflict !!!! interruption off by neopixel
      }
    }
}


void init_car( car_t* car, controller_t* ct, uint32_t color ,float fuel){
  car->ct = ct;
  car->color = color;
  car->trackID = TRACK_MAIN;
  car->speed=0;
  car->dist=0;
  car->dist_aux=0;
  car->fuel=fuel;
}


void init_controller( controller_t* ct, enum ctr_type mode, int pin,int pin_status ){
  ct->mode = mode;
  ct->pin = pin;
  ct->delta_analog = DELTA_ANALOG;
  ct->pin_status= pin_status;
}


byte get_controllerStatus_old( controller_t* ct ) {
  if( ct->mode == DIGITAL_MODE ){
    return digitalRead( ct->pin );
  }
  else if( ct->mode == ANALOG_MODE ){
    ct->adc = analogRead( ct->pin );
    if( abs( ct->badc - ct->adc ) > ct->delta_analog ){
      ct->badc = ct->adc;
      return 0;
    }else{
      ct->badc = ct->adc;
      return 1;
    }
  }
  return 0;

}

byte get_controllerStatus( controller_t* ct ) {
  if( ct->mode == DIGITAL_MODE ){
    return digitalRead( ct->pin );
  }
  else if( ct->mode == ANALOG_MODE ){
       ct->adc = analogRead( ct->pin );
       //Serial.print( ct->adc);
       //Serial.print( " ");       
       if ((ct->adc ) < MIN_ADC1)
       {//Serial.println( "0");
       return 1;
       }else 
       if ((ct->adc ) > MAX_ADC1)
              {//Serial.println( "1");
              return 0;}
              else {//Serial.println( "-1");
                    return -1;}
              };               
}


void draw_car( car_t* car ) {//for(int i=0;i<=loop1;i++){track.setPixelColor(((word)dist1 % NPIXELS)+i, track.Color(0,255-i*20,0));};
  switch ( car->trackID ){
    case TRACK_MAIN:
      //track.setPixelColor( ((word)car->dist % NPIXELS),car->color );
      //track.setPixelColor( ((word)car->dist % NPIXELS)+1, car->color);
   for (int i=0;i<(car->loop)+1;i++) {track.setPixelColor( ((word)car->dist % NPIXELS)+i,car->color );}; 
      
    break;
    case TRACK_AUX:
      track.setPixelColor( (word)(NPIXELS + car->dist_aux), car->color);
      track.setPixelColor( (word)(NPIXELS + car->dist_aux)+1, car->color);      
    break;
    case TRACK_IN:
      track.setPixelColor(((word)car->dist_in % NPIXELS)+1, car->color);
    break;
    case TRACK_OUT:
      track.setPixelColor(((word)car->dist_out % NPIXELS)+1, car->color);
    break;
  }
}


void process_aux_track( car_t* car ){

  controller_t* ct = car->ct;
  if ( (ct->flag_sw == 1 ) && (get_controllerStatus( ct ) == 0) ) {
    ct->flag_sw = 0;
    car->speed += ACEL;
    digitalWrite(car->ct->pin_status,HIGH);
  }
  if ( (ct->flag_sw == 0 ) && (get_controllerStatus( ct ) ==1 ) ) {
    ct->flag_sw = 1;
    digitalWrite(car->ct->pin_status,LOW);
  }
  if ( ((int)car->dist_aux == LED_SPEED_COIN)
          //&& (car->speed <= ACEL) // quito requisito de baja velocidad 
          //&& (ct->flag_sw == 0) ) //quito requisito de switch pulsado
          )
          {
    car->speed = ACEL*10;
    digitalWrite(PIN_REL1,LOW);
    digitalWrite(PIN_REL2,LOW);
    LED_SPEED_COIN = -1;
    T_SPEED_COIN = millis() + random(5000,30000);
  };
  car->speed -= car->speed*kf;
  car->dist_aux += car->speed;
  if ( car->dist_aux > NLED_AUX_TRACK ) {
    car->trackID =TRACK_MAIN;
    //car->dist += NLED_AUX_TRACK;  //caso NLED_AUX_TRACK == END_TRACK_AUX
    car->dist += END_TRACK_AUX;  //caso NLED_AUX_TRACK != END_TRACK_AUX
    digitalWrite(PIN_REL1,HIGH);
    digitalWrite(PIN_REL2,HIGH);
  }
}


void update_track( car_t* car ){
  controller_t* ct = car->ct;
  //Serial.println ( ((int)car->dist) % NPIXELS);
  if ((SELECT_TRACK_AUX==1) && ( car->trackID == TRACK_MAIN ) 
      && (  ((((int)car->dist) % NPIXELS)+1) >= INI_TRACK_AUX)        
     // && ( get_controllerStatus( ct ) == 0) ) // cambio de track por switch
      && ( car->speed < SPEED_TAUX) )   // cambio por velocidad
     {
     // car->trackID = TRACK_AUX;   //lo quito para no entrar a track aux
     // car->dist_aux = 0;
     }
}


void process_main_track( car_t* car ){

  controller_t* ct = car->ct;
  if ( (ct->flag_sw == 1 ) && (get_controllerStatus( ct ) == 0) ) {
    ct->flag_sw = 0;
    car->speed += ACEL;
    //track.setPixelColor(1,track.Color(255,255,255));
    digitalWrite(car->ct->pin_status,HIGH);
  }
  if ( (ct->flag_sw == 0 ) && (get_controllerStatus( ct ) ==1 ) ) { digitalWrite(car->ct->pin_status,LOW);
    ct->flag_sw = 1;
  }
  if ( gravity_map[(word)car->dist % NPIXELS] < 127 )
    car->speed -= kg*(127-(gravity_map[(word)car->dist % NPIXELS]) );
  if ( gravity_map[(word)car->dist % NPIXELS] > 127 )
    car->speed += kg*((gravity_map[(word)car->dist % NPIXELS])-127 );
  car->speed -= car->speed*kf;
  car->dist += car->speed;
}


#ifdef FALSE

void burning1(){
//to do
 }

void burning2(){
//to do
 }

void track_rain_fx(){
//to do
 }

void track_oil_fx(){
//to do
 }

void track_snow_fx(){
//to do
 }


void fuel_empty(){
//to do
 }

void fill_fuel_fx(){
//to do
 }

void in_track_boxs_fx(){
//to do
 }

void pause_track_boxs_fx(){
//to do
 }

void flag_boxs_stop(){
//to do
 }

void flag_boxs_ready(){
//to do
 }

void draw_safety_car(){
//to do
 }

void telemetry_rx(){
  //to do
 }

void telemetry_tx(){
  //to do
 }

void telemetry_lap_time_car1(){
//to do
 }

void telemetry_lap_time_car2(){
//to do
 }

void telemetry_record_lap(){
//to do
 }

void telemetry_total_time(){
//to do
 }

int read_sensor(byte player){
//to do
}

int calibration_sensor(byte player){
  //to do
}

int display_lcd_laps(){
  //to do
}

int display_lcd_time(){
  //to do
}

void fx_tunel_in(byte car)
{
}

void fx_tunel_out(byte car)
{
}

#endif
